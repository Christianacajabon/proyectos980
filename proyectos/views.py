from django.http import HttpResponse
from datetime import datetime
import json

def primera_pagina(request):
    hora = datetime.now().strftime('%b %d th, %Y - %H:%M hrs')
    return HttpResponse('Primera pagina. Y la hora es {hora}'.format(hora=hora))



def debbuger_primario(request):
    numero = request.GET['numeros']
    import pdb; pdb.set_trace()
    return HttpResponse(numero)